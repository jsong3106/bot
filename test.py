
import sys
from PyQt5.QtWidgets import QApplication
from library.open_api import open_api



if __name__ == "__main__":
    print("__main__에 들어왔습니다.")
    # 아래는 키움증권 openapi를 사용하기 위해 사용하는 한 줄! 이해 할 필요 X
    app = QApplication(sys.argv)

    o = open_api()
    o.check_balance()
